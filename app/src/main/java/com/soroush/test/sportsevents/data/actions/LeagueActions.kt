package com.soroush.test.sportsevents.data.actions

import com.soroush.test.sportsevents.data.model.League
import com.soroush.test.sportsevents.data.model.Team
import com.soroush.test.sportsevents.data.service.LeagueService
import com.soroush.test.sportsevents.data.service.TeamService
import com.soroush.test.sportsevents.viewmodel.viewinterface.LeagueView
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import java.util.*

class LeagueActions(private val leagueView: LeagueView) : BaseAction() {
    fun getLeaguesForSportName(sportName: String): Disposable {
        return LeagueService.getInstance().allLeagues
            .subscribe(
                { leagues: ArrayList<League>? ->
                    leagues?.filter { it.strSport == sportName }?.let {
                        leagueView.setLeagues(it)
                        if (it.isNotEmpty()) leagueView.setSelectedLeague(it.first())
                    }

                }, ::logError
            )
    }

    fun populateLeagueDetails(leagues: List<League>) {
        for ((idLeague) in leagues) {
            val d = LeagueService.getInstance().getLeagueDetail(idLeague)
                .subscribe({ league: League? ->
                    league?.let { leagueView.updateLeagueDetail(it) }
                }, ::logError)
        }
    }

    fun getTeamsForLeague(leagueName: String): Disposable {
        return TeamService.getInstance().getTeamsByLeagueName(leagueName)
            .subscribe(
                { teams: List<Team>? ->
                    teams?.let {
                        leagueView.setLeagueTeams(teams)
                        val teamLogoMap: MutableMap<Int, String> = HashMap()
                        for (team in teams)
                            teamLogoMap[team.idTeam] = team.strTeamBadge

                        leagueView.setTeamLogos(teamLogoMap)
                    }
                }, ::logError
            )
    }
}

package com.soroush.test.sportsevents.data.service

import android.content.Context

object Services {
    @JvmStatic
    fun instantiate(context: Context) {
        EventsService.instantiate(context)
        LeagueService.instantiate(context)
        TeamService.instantiate(context)
        SportService.instantiate(context)
    }
}
package com.soroush.test.sportsevents.data.db

import android.content.Context
import androidx.room.Room
import com.soroush.test.sportsevents.helper.Constants

object Db {
    @JvmStatic
    @Throws(IllegalStateException::class)
    fun getInstance(): Database? {
        checkNotNull(instance) { Constants.NOT_INSTANTIATED }
        return instance
    }

    fun instantiate(context: Context) {
        instance = Room.databaseBuilder(context, Database::class.java, Constants.DB_NAME).build()
    }

    private var instance: Database? = null
}